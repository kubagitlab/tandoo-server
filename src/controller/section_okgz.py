from src.controller import entity
from src.models import SectionOkgz, db, DirOkgz


def all(data):
    return SectionOkgz.get_by(section_id=data.get('section_id'))


def get_entities(data):
    if data.get('id'):
        return entity.all({"parent_id": data['id']})

    section_id = data['section_id']
    okgz_id = data['okgz_id']

    so = SectionOkgz.get_by_first(section_id=section_id, okgz_id=okgz_id)
    if not so:
        return []

    return entity.all({"parent_id": so.id})


def get_okgz(data):
    okgz_list = db.session.query(DirOkgz).select_from(SectionOkgz) \
        .outerjoin(DirOkgz, SectionOkgz.okgz_id == DirOkgz.id).filter(SectionOkgz.section_id == data['section_id']).all()

    return okgz_list
