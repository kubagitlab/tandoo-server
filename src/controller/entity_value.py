from src.common.error_codes import GENERIC_ERROR
from src.common.exceptions import CoreException
from src.models import EntityValue, Entity, ProductValue


def all(data):
    entity_id = data.get('entity_id')
    return EntityValue.get_by(entity_id=entity_id) if entity_id else EntityValue.all()


def get(data):
    return EntityValue.get(data.get('id'))


def delete(id):
    ev = EntityValue.get(id)
    product_value_check(ev)
    ev.delete()


def delete_by_entity_id(entity_id):
    values = EntityValue.get_by(entity_id=entity_id)
    for value in values:
        product_value_check(value)
        value.delete()


def product_value_check(ev):
    entity = Entity.get(ev.entity_id)
    pv = ProductValue.get_by_first(entity_id=entity.id, value_id=ev.id)
    if pv and not entity.spr:
        raise CoreException(GENERIC_ERROR, 'Product value is linked: {}'.format(ev.id))
