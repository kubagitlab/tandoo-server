from flask import g
from sqlalchemy import or_

from ..common.utils import *
from ..models import DirOkgz, db


def search(bag):
    val = bag['search']
    if not val:
        return []
    search_like = '%{}%'.format(val)
    okgz_data = db.session.query(DirOkgz).filter(or_(DirOkgz.name.like(search_like), DirOkgz.code.like(search_like))).all()
    return okgz_data
