import importlib

from flask import g

from ..common.utils import *
from ..models import DirCountry, DirMeasureUnit, DirBank, DirOwnership, DirPosition, DirOkgz, DirProcurement, \
    DirProcureType, DirProcureFormat, DirProcureStatus, DirSingleSource, DirSingleSrcRsn

PDIR = ['DirCountry', 'DirMeasureUnit', 'DirBank', 'DirOwnership', 'DirPosition', 'DirOkgz', 'DirProcurement',
        'DirProcureType', 'DirProcureFormat', 'DirProcureStatus', 'DirSingleSource', 'DirSingleSrcRsn']
DIR_PREFIX = 'dir'
DIR_KEY = 'dir:{}'


def dir_country(key):
    data = eds_get('countries/all', {})
    for d in data:
        dc = DirCountry.get_by_first(ref_id=d['id'])
        if not dc:
            dc = DirCountry()
        dc.ref_id = d['id']
        dc.name = d['titleRu']
        dc.name_kg = d['titleKy']
        dc.name_en = d['titleEn']
        dc.iso_code = d['isoCode']
        dc.sorting = d['sorting']
        dc.save()
    data = orm_to_json(DirCountry.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_bank(key):
    data = eds_get('bank/all', {})
    for d in data:
        dbnk = DirBank.get_by_first(ref_id=d['id'])
        if not dbnk:
            dbnk = DirBank()
        dbnk.ref_id = d['id']
        dbnk.name = d['name']
        dbnk.address = d['address']
        dbnk.phoneNumber = d['phoneNumber']
        dbnk.head = d['head']
        dbnk.numberOfSubDivisions = d['numberOfSubDivisions']
        dbnk.email = d['email']
        dbnk.website = d['website']
        dbnk.save()
    data = orm_to_json(DirBank.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_ownership(key):
    data = eds_get('legalForm/all', {})
    for d in data:
        do = DirOwnership.get_by_first(ref_id=d['id'])
        if not do:
            do = DirOwnership()
        do.ref_id = d['id']
        do.name = d['titleRu']
        do.name_kg = d['titleKy']
        do.name_en = d['titleEn']
        do.type_owner = d['type']
        do.save()
    data = orm_to_json(DirOwnership.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_position(key):
    data = eds_get('position/all', {})
    for d in data:
        dps = DirPosition.get_by_first(ref_id=d['id'])
        if not dps:
            dps = DirPosition()
        dps.ref_id = d['id']
        dps.name = d['name']
        dps.name_kg = d['code']
        dps.name_en = d['name_en']
        dps.order = d['order']
        dps.save()
    data = orm_to_json(DirPosition.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_measure_unit(key):
    data = eds_get('measurementUnits/all', {})
    for d in data:
        mu = DirMeasureUnit.get_by_first(ref_id=d['id'])
        # print(mu)
        if not mu:
            mu = DirMeasureUnit()
        mu.ref_id = d['id']
        mu.name = d['fullName']
        mu.short_name = d['shortNameRU']
        mu.short_name_en = d['shortNameEN']
        mu.type = d['measurementUnitType']
        mu.group = d['measurementUnitGroup']
        mu.save()
    data = orm_to_json(DirMeasureUnit.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_okgz(key):
    data = eds_get('okgz/all', {})
    check = DirOkgz.count() != 0
    for d in data:
        o = None
        if check:
            o = DirOkgz.get_by_first(ref_id=d['id'])
        if not o:
            o = DirOkgz()
        o.ref_id = d['id']
        o.name = d['name']
        o.code = d['codeName']
        o.parent_id = d['parent_id']
        o.save()

    data = orm_to_json(DirOkgz.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_procurement(key):
    data = eds_get('enum/procurementMethod', {})
    check = DirProcurement.count() != 0
    for d in data["procurementMethod"]:
        dpm = None
        if check:
            dpm = DirProcurement.get_by_first(code=d)
        if not dpm:
            dpm = DirProcurement()
        dpm.name = data["procurementMethod"][d]
        dpm.code = d
        dpm.save()
    data = orm_to_json(DirProcurement.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_procure_type(key):
    data = eds_get('enum/orderType', {})
    check = DirProcureType.count() != 0
    for d in data["orderType"]:
        dpt = None
        if check:
            dpt = DirProcureType().get_by_first(code=d)
        if not dpt:
            dpt = DirProcureType()
        dpt.name = data["orderType"][d]
        dpt.code = d
        dpt.save()
    data = orm_to_json(DirProcureType.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_procure_format(key):
    data = eds_get('enum/orderFormat', {})
    check = DirProcureFormat.count() != 0
    for d in data["orderFormat"]:
        dpf = None
        if check:
            dpf = DirProcureFormat().get_by_first(code=d)
        if not dpf:
            dpf = DirProcureFormat()
        dpf.name = data["orderFormat"][d]
        dpf.code = d
        dpf.save()
    data = orm_to_json(DirProcureFormat.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_procure_status(key):
    data = eds_get('enum/orderStatus', {})
    check = DirProcureStatus.count() != 0
    for d in data["orderStatus"]:
        dps = None
        if check:
            dps = DirProcureStatus().get_by_first(code=d)
        if not dps:
            dps = DirProcureStatus()
        dps.name = data["orderStatus"][d]
        dps.code = d
        dps.save()
    data = orm_to_json(DirProcureStatus.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_single_source(key):
    data = eds_get('enum/singleSourceContract', {})
    check = DirSingleSource.count() != 0
    for d in data["singleSourceContract"]:
        dss = None
        if check:
            dss = DirSingleSource().get_by_first(code=d)
        if not dss:
            dss = DirSingleSource()
        dss.name = data["singleSourceContract"][d]
        dss.code = d
        dss.save()
    data = orm_to_json(DirSingleSource.all())
    g.redis.set(key, json.dumps(data))
    return data


def dir_single_src_rsn(key):
    data = eds_get('enum/singleSourceReason', {})
    check = DirSingleSrcRsn.count() != 0
    for d in data["singleSourceReason"]:
        ssr = None
        if check:
            ssr = DirSingleSrcRsn().get_by_first(code=d)
        if not ssr:
            ssr = DirSingleSrcRsn()
        ssr.name = data["singleSourceReason"][d]
        ssr.code = d
        ssr.save()
    data = orm_to_json(DirSingleSrcRsn.all())
    g.redis.set(key, json.dumps(data))
    return data


def all():
    module = importlib.import_module('src.models')
    models = []
    for attr in dir(module):
        if attr.startswith('Dir'):
            models.append(attr)

    dir_data = []
    for m in models:
        model = getattr(module, m)
        table_name = model.__tablename__
        columns = []
        for column in model.metadata.tables.get(table_name).columns._data:
            if column not in ['created_date', 'ref_id']:
                columns.append(column)
        t = {
            'table': m,
            'columns': columns
        }
        dir_data.append(t)

    return dir_data


def listing(bag):
    dir_name = bag['name']
    key = DIR_KEY.format(dir_name)
    data = g.redis.get(key)
    # data = None
    if data:
        return json.loads(data)

    if dir_name in PDIR:
        if dir_name == 'DirCountry':
            return dir_country(key)
        elif dir_name == 'DirMeasureUnit':
            return dir_measure_unit(key)
        elif dir_name == 'DirBank':
            return dir_bank(key)
        elif dir_name == 'DirOwnership':
            return dir_ownership(key)
        elif dir_name == 'DirPosition':
            return dir_position(key)
        elif dir_name == 'DirOkgz':
            return dir_okgz(key)
        elif dir_name == 'DirProcurement':
            return dir_procurement(key)
        elif dir_name == 'DirProcureType':
            return dir_procure_type(key)
        elif dir_name == 'DirProcureFormat':
            return dir_procure_format(key)
        elif dir_name == 'DirProcureStatus':
            return dir_procure_status(key)
        elif dir_name == 'DirSingleSource':
            return dir_single_source(key)
        elif dir_name == 'DirSingleSrcRsn':
            return dir_single_src_rsn(key)
    else:
        data = orm_to_json(get_model_all(dir_name))
        if data:
            g.redis.set(key, json.dumps(data))
        return data


def save(bag):
    table_name = bag['name']
    data = bag['data']
    model = get_model(table_name)
    id = data.get('id')
    t = model.get(id) if id else model()
    t.from_json(data)
    t.save()

    data = orm_to_json(model.all())
    key = DIR_KEY.format(table_name)
    if data:
        g.redis.set(key, json.dumps(data))

    return data


def update(bag):
    if bag['key'] == 'all':
        search_key = '{}:*'.format(DIR_PREFIX)
        for key in g.redis.scan_iter(search_key):
            g.redis.delete(key)
    else:
        key = DIR_KEY.format(bag['key'])
        g.redis.delete(key)
