from flask import g
from sqlalchemy import text, null
from sqlalchemy.orm import aliased

from src.common.constants import *
from src.common.error_codes import GENERIC_ERROR
from src.common.utils import *
from src.models import *


def all(data):
    parent_id = data.get('parent_id')
    products = Product.get_by(parent_id=parent_id) if parent_id else []

    arr = []
    for p in products:
        l = p.to_json()
        # l.pop('values')
        values = p.values
        for v in values:
            entity = Entity.get(v.entity_id)
            if entity.spr:
                entity_value = get_model_by(entity.dir_name, v.value_id)
                l[entity.name] = entity_value.name
            else:
                entity_value = EntityValue.get(v.value_id)
                l[entity.name] = entity_value.name

        arr.append(l)
    return arr


def save(product):
    p = Product.get(product['id']) if product.get('id', False) else Product()
    p.from_json(product)
    # if not p.barcode:
    #     raise CoreException(GENERIC_ERROR, u'Введите Штрих код')
    if not p.image:
        raise CoreException(GENERIC_ERROR, u'Изображение не загружено')
    p.user_id = g.user.id
    p.save()
    db.session.flush()
    for value in product['values']:
        value['product_id'] = p.id
        if not value['value_id']:
            raise CoreException(GENERIC_ERROR, u'Введите поле: {}'.format(value['label']))

        v = ProductValue.get(value['id']) if value.get('id', False) else ProductValue()
        v.from_json(value)
        v.save()
        db.session.flush()
    return


def get(id):
    product = Product.get(id)
    p = product.to_json()
    p['values'] = []
    for value in product.values:
        entity = Entity.get(value.entity_id)
        v = prepare_value(value, entity)
        if v:
            p['values'].append(v)
        if hasattr(g, 'user') and g.user.role_id == ROLE_PURCHASER:
            avg_price = db.session.query(func.sum(CompanyProduct.unit_price) / func.count()) \
                .filter(CompanyProduct.product_id == product.id).group_by(CompanyProduct.product_id).first()
            p['avg_price'] = round(avg_price[0], 2)

    return p


def prepare_value(product_value, entity):
    visible = False

    authorized = hasattr(g, 'user') and g.user

    if not authorized and entity.guest:
        visible = True
    if authorized and g.user.role_id in [ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_OPERATOR]:
        visible = True
    if authorized and g.user.role_id in [ROLE_PURCHASER] and entity.purchaser:
        visible = True
    if authorized and g.user.role_id in [ROLE_SUPPLIER] and entity.supplier:
        visible = True

    val = None
    if visible:
        val = product_value.to_json()
        val['name'] = entity.name
        if entity.spr:
            entity_value = get_model_by(entity.dir_name, product_value.value_id)
        else:
            entity_value = EntityValue.get(product_value.value_id)
        val['value'] = entity_value.name

    return val


def products_by_category(bag):
    okgz_id = bag['okgz_id']
    products, sel_specs, products_id = [], [], []

    specs = bag.get('filters', {}) and bag['filters'].get('specs', [])
    dirs = bag.get('filters', {}) and bag['filters'].get('dirs', [])
    filters = [*specs, *dirs]
    if filters:
        query_where = ''
        counter = 1
        for s in filters:
            qwh = u'(entity_id = {} and value_id = {})'.format(s['entity_id'], s['value_id'])
            if counter == 1:
                query_where = qwh
            else:
                query_where = u'{} or {}'.format(query_where, qwh)
            counter += 1

        if hasattr(g, 'user') and g.user.role_id == ROLE_SUPPLIER:
            cp = u'(select product_id from company_product where product_id = pr.product_id and company_id = {}) "cp"' \
                .format(g.company.id)
        else:
            cp = u'(select null) "cp"'

        query = text("""
        with prods as (
            SELECT count(product_id), product_id
            FROM product_value
            WHERE """ + query_where + """
            group by product_id
            having count(product_id) = :filter_count
        )
        select *, """ + cp + """
        from prods pr
                 inner join product p on p.id = pr.product_id
        order by p.barcode;
        """)
        query = query.bindparams(filter_count=len(filters))
        result = db.session.execute(query)

        for item in result:
            p = dict(item.items())
            p['exist'] = True if p.get('cp', '') else False
            products.append(p)
            products_id.append(p['id'])
    else:
        if hasattr(g, 'user') and g.user.role_id == ROLE_SUPPLIER:
            sql = db.session.query(Product,
                                   (db.session.query(CompanyProduct.id).select_from(CompanyProduct)
                                    .filter(CompanyProduct.product_id == Product.id,
                                            CompanyProduct.company_id == g.company.id).limit(1)).label('cp'))
        else:
            sql = db.session.query(Product, null())
        sql = sql.outerjoin(SectionOkgz, Product.parent_id == SectionOkgz.id) \
            .outerjoin(DirOkgz, SectionOkgz.okgz_id == DirOkgz.id) \
            .filter(SectionOkgz.okgz_id == okgz_id) \
            .order_by(Product.barcode)

        for p, cp in sql.all():
            pr = orm_to_json(p)
            pr['exist'] = True if cp else False
            products.append(pr)
            products_id.append(p.id)

    filters = {'specs': get_specs(okgz_id, products_id), 'dirs': get_dirs(okgz_id, products_id)}
    return {'products': products, 'filters': filters}


def get_specs(okgz_id, products_id):
    sql = db.session.query(ProductValue.entity_id, Entity.name, EntityValue.id, EntityValue.name) \
        .select_from(Product) \
        .join(SectionOkgz, Product.parent_id == SectionOkgz.id) \
        .join(DirOkgz, SectionOkgz.okgz_id == DirOkgz.id) \
        .join(ProductValue, Product.id == ProductValue.product_id) \
        .join(Entity, ProductValue.entity_id == Entity.id) \
        .join(EntityValue, ProductValue.value_id == EntityValue.id) \
        .filter(SectionOkgz.okgz_id == okgz_id, Entity.spr == False)

    if products_id:
        sql = sql.filter(Product.id.in_(products_id))
    sql = sql.group_by(ProductValue.entity_id, Entity.name, EntityValue.id, EntityValue.name) \
        .order_by(ProductValue.entity_id)
    specs_data = sql.all()

    curr_entity_id = counter = 0
    specs, values, sp = [], [], {}
    for s in specs_data:
        counter += 1
        if curr_entity_id == 0:
            curr_entity_id = s[0]
        if curr_entity_id != s[0]:
            sp['values'] = values
            specs.append(sp)
            curr_entity_id = s[0]
            sp = {'id': s[0], 'name': s[1]}
            values = [{'id': s[2], 'name': s[3]}]
        else:
            values.append({'id': s[2], 'name': s[3]})
            if not sp:
                sp = {'id': s[0], 'name': s[1]}
        if counter == len(specs_data):
            sp['values'] = values
            specs.append(sp)
    return specs


def get_dirs(okgz_id, products_id):
    sql = db.session.query(ProductValue.entity_id, Entity.name, ProductValue.value_id, Entity.dir_name) \
        .select_from(Product) \
        .join(SectionOkgz, Product.parent_id == SectionOkgz.id) \
        .join(DirOkgz, SectionOkgz.okgz_id == DirOkgz.id) \
        .join(ProductValue, Product.id == ProductValue.product_id) \
        .join(Entity, ProductValue.entity_id == Entity.id) \
        .filter(SectionOkgz.okgz_id == okgz_id, Entity.spr == True)

    if products_id:
        sql = sql.filter(Product.id.in_(products_id))
    sql = sql.group_by(ProductValue.entity_id, Entity.name, ProductValue.value_id, Entity.dir_name) \
        .order_by(ProductValue.entity_id)
    dirs_data = sql.all()

    curr_entity_id = counter = 0
    dirs, values, dr = [], [], {}
    for d in dirs_data:
        counter += 1
        if curr_entity_id == 0:
            curr_entity_id = d[0]
        if curr_entity_id != d[0]:
            dr['values'] = values
            dirs.append(dr)
            curr_entity_id = d[0]
            dr = {'id': d[0], 'name': d[1]}
            v = get_model_by(d[3], d[2])
            values = [{'id': d[2], 'name': d[3], 'value': v.name}]
        else:
            if not dr:
                dr = {'id': d[0], 'name': d[1]}

            v = get_model_by(d[3], d[2])
            values.append({'id': d[2], 'name': d[3], 'value': v.name})
        if counter == len(dirs_data):
            dr['values'] = values
            dirs.append(dr)
    return dirs


def get_sections():
    sections = []
    data = db.session.query(SectionOkgz.section_id, DirSection.name, SectionOkgz.okgz_id, DirOkgz.name) \
        .select_from(CompanyProduct) \
        .join(Product, CompanyProduct.product_id == Product.id) \
        .join(SectionOkgz, Product.parent_id == SectionOkgz.id) \
        .join(DirOkgz, SectionOkgz.okgz_id == DirOkgz.id) \
        .join(DirSection, SectionOkgz.section_id == DirSection.id) \
        .group_by(SectionOkgz.section_id, DirSection.name, SectionOkgz.okgz_id, DirOkgz.name) \
        .order_by(SectionOkgz.section_id, SectionOkgz.okgz_id).all()

    curr_section_id = counter = 0
    categories, s = [], {}
    for d in data:
        counter += 1

        if counter == 1:
            curr_section_id = d[0]
        if curr_section_id != d[0]:
            s['categories'] = categories
            sections.append(s)
            curr_section_id = d[0]
            s = {'id': d[0], 'name': d[1]}
            categories = [{'id': d[2], 'name': d[3]}]
        else:
            categories.append({'id': d[2], 'name': d[3]})
            if not s:
                s = {'id': d[0], 'name': d[1]}

        if counter == len(data):
            s['categories'] = categories
            sections.append(s)

    return sections


def company_products(bag):
    okgz_id = bag['category_id']
    products, sel_specs, products_id = [], [], []

    specs = bag.get('filters', {}) and bag['filters'].get('specs', [])
    dirs = bag.get('filters', {}) and bag['filters'].get('dirs', [])
    filters = [*specs, *dirs]
    if filters:
        query_where = ''
        counter = 1
        for s in filters:
            qwh = u'(entity_id = {} and value_id = {})'.format(s['entity_id'], s['value_id'])
            if counter == 1:
                query_where = qwh
            else:
                query_where = u'{} or {}'.format(query_where, qwh)
            counter += 1

        query = text("""
        with prods as (
            with v as (
                SELECT distinct pv.*
                FROM company_product cp
                         inner join product_value pv on cp.product_id = pv.product_id
            )
            SELECT v.product_id, count(v.product_id)
            FROM v
            WHERE """ + query_where + """
            group by v.product_id
            having count(v.product_id) = :filter_count
        )
        select (select round((sum(unit_price) / count(*))::numeric, 2) 
                from company_product
                where product_id = pr.product_id) as "avg_price", p.*
        from prods pr
                 inner join product p on p.id = pr.product_id
         order by p.barcode;
        """)
        query = query.bindparams(filter_count=len(filters))
        result = db.session.execute(query)

        for item in result:
            p = dict(item.items())
            products.append(p)
            products_id.append(p['id'])
    else:
        cp = aliased(CompanyProduct)
        sql = db.session.query(Product,
                               (db.session.query(func.sum(cp.unit_price) / func.count())
                                .select_from(cp).filter(cp.product_id == Product.id)
                                .group_by(cp.product_id).limit(1)).label('avg_price')) \
            .select_from(CompanyProduct) \
            .join(Product, CompanyProduct.product_id == Product.id) \
            .join(SectionOkgz, Product.parent_id == SectionOkgz.id) \
            .filter(SectionOkgz.okgz_id == okgz_id) \
            .order_by(Product.barcode)

        for p, prc in sql.all():
            pr = p.to_json()
            pr['avg_price'] = round(prc, 2)
            products.append(pr)
            products_id.append(p.id)

    filters = {'specs': get_specs(okgz_id, products_id), 'dirs': get_dirs(okgz_id, products_id)}
    return {'products': products, 'filters': filters}
