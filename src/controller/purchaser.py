from ..common.constants import ADVERT_STATUS_DRAFT, ADVERT_STATUS_PUBLISHED
from ..common.error_codes import USER_NOT_AUTHORIZED
from ..common.utils import *
from ..models import *


def save_advert(bag):
    bag_advert = bag['announce']
    if bag_advert.get('id', ''):
        a = Advert.get(bag_advert['id'])
    else:
        a = Advert()
        a.status = ADVERT_STATUS_DRAFT

    a.dir_section_id = bag_advert['section']
    a.dir_procurement_id = bag_advert['procurement']
    a.company_id = g.company.id
    # a.company_id = 5  # for test
    a.update_date = datetime.datetime.now()

    a.save()
    db.session.flush()
    advert_id = a.id

    for l in bag_advert['lots']:
        if l.get('id', ''):
            lot = AdvertLot().get(l['id'])
        else:
            lot = AdvertLot()

        params = l['params']
        lot.advert_id = advert_id
        lot.company_id = g.company.id
        # lot.company_id = 5  # for test
        lot.dir_okgz_id = l['category']
        lot.dir_unit_id = l['unit']
        lot.unit_price = params['unit_price']
        lot.estimated_delivery_time = params['estimated_delivery_time']
        lot.quantity = params['quantity']
        lot.budget = format(params['quantity'] * params['unit_price'], '.2f')
        lot.delivery_place = ''

        lot.save()
        db.session.flush()
        lot_id = lot.id

        for s in l['specs']:
            ls = LotSpec().get_by_first(advert_lot_id=lot_id, entity_id=s['entity_id'], value_id=s['value_id'])
            if not ls:
                ls = LotSpec()
            ls.advert_lot_id = lot_id
            ls.entity_id = s['entity_id']
            ls.value_id = s['value_id']
            ls.save()

        for d in l['dirs']:
            ls = LotDir().get_by_first(advert_lot_id=lot_id, entity_id=d['entity_id'], value_id=d['value_id'])
            if not ls:
                ls = LotDir()
            ls.advert_lot_id = lot_id
            ls.entity_id = d['entity_id']
            ls.value_id = d['value_id']
            ls.save()
    return {'id': advert_id}


def advert_list(bag):
    if not hasattr(g, 'company'):
        raise CoreException(u'У вас нет организации')
    company_id = g.company.id
    # company_id = 5
    adverts = []
    sql = db.session.query(Advert, DirSection, (db.session.query(func.coalesce(func.sum(AdvertLot.budget), 0))
                                                .filter(AdvertLot.advert_id == Advert.id).limit(1)).label('budget')) \
        .join(DirSection, Advert.dir_section_id == DirSection.id) \
        .filter(Advert.company_id == company_id)

    filters = bag.get('filter', '')
    if filters:
        if filters.get('status', ''):
            sql = sql.filter(Advert.status == filters['status'])
    sql = sql.order_by(Advert.created_date.desc())

    for a, s, b in sql.all():
        ad = a.to_json()
        ad['section'] = s.name
        ad['budget'] = b
        adverts.append(ad)

    return adverts


def get_advert(bag):
    advert_sql = db.session.query(Advert, DirSection, DirProcurement) \
        .join(DirSection, Advert.dir_section_id == DirSection.id) \
        .join(DirProcurement, Advert.dir_procurement_id == DirProcurement.id) \
        .filter(Advert.id == bag['id'])
    if hasattr(g, 'company'):
        advert_sql = advert_sql.filter(Advert.company_id == g.company.id)
    advert, ds, dp = advert_sql.first()
    advert.section = ds
    advert.procurement = dp

    lot_sql = db.session.query(AdvertLot, DirOkgz, DirMeasureUnit) \
        .join(DirOkgz, AdvertLot.dir_okgz_id == DirOkgz.id) \
        .join(DirMeasureUnit, AdvertLot.dir_unit_id == DirMeasureUnit.id) \
        .filter(AdvertLot.advert_id == advert.id)

    lots = []
    for l, do, dm in lot_sql.all():
        l.category = do
        l.unit = dm
        l.address = l.data.get('address', {})

        spec_sql = db.session.query(LotSpec, Entity, EntityValue) \
            .join(Entity, LotSpec.entity_id == Entity.id) \
            .join(EntityValue, LotSpec.value_id == EntityValue.id) \
            .filter(LotSpec.advert_lot_id == l.id)
        specs = []
        for s, e, ev in spec_sql.all():
            s.entity_name = e.name
            s.value_name = ev.name
            specs.append(s)

        dir_sql = db.session.query(LotDir, Entity) \
            .join(Entity, LotDir.entity_id == Entity.id) \
            .filter(LotDir.advert_lot_id == l.id)
        dirs = []
        for d, e in dir_sql.all():
            d.entity = e.name
            d.value_name = get_dir_value(e.dir_name, d.value_id)
            dirs.append(d)

        l.specs = specs
        l.dirs = dirs
        lots.append(l)

    advert.lots = lots

    return advert


def update_advert_status(bag):
    advert = db.session.query(Advert).filter_by(id=bag['id']).first()
    if not advert:
        pass
    advert.status = bag['status']
    advert.save()

    return {'id': advert.id}


def published_adverts(bag):
    adverts = []
    sql = db.session.query(Advert, DirSection, DirProcurement) \
        .join(DirSection, Advert.dir_section_id == DirSection.id) \
        .join(DirProcurement, Advert.dir_procurement_id == DirProcurement.id) \
        .filter(Advert.status == ADVERT_STATUS_PUBLISHED)

    sql = sql.order_by(Advert.created_date.desc())

    for a, ds, dp in sql.all():
        ad = a.to_json()
        ad['section'] = ds.name
        ad['procurement'] = dp.name
        adverts.append(ad)

    return adverts
