from datetime import datetime

from flask import g
from sqlalchemy.orm.attributes import flag_modified

from ..common.constants import ROLE_SUPPLIER, COMPANY_STATUS_DRAFT, COMPANY_STATUS_WAITING, COMPANY_STATUS_CONFIRMED
from ..common.error_codes import RECORD_NOT_FOUND, GENERIC_ERROR
from ..common.exceptions import CoreException
from ..common.utils import eds_post
from ..models import Company, CompanyQualification, User


def save(bag):
    c = bag['company']
    comp = Company().get_by_first(ref_id=c['id'])
    if not comp:
        company_type = None
        if bag['role']['roleType'] == ROLE_SUPPLIER:
            company_type = 'supplier'
        elif bag['role']['roleType'] == ROLE_SUPPLIER:
            company_type = 'purchaser'

        comp = Company()
        comp.ref_id = c['id']
        comp.user_id = bag['user_id']
        comp.inn = c['companyInn']
        comp.name = c['title']
        comp.short_name = c['title']
        comp.company_status = COMPANY_STATUS_DRAFT
        comp.company_type = company_type
        comp.save()
    return comp


def get_all():
    return Company.all()


def get_info_from_portal(bag):
    c = None
    if bag.get('company_id'):
        c = Company.get(bag['company_id'])
        u = User.get(c.user_id)
        params = {'companyInn': c.inn, 'userPin': u.inn, 'username': u.username}
    else:
        params = {'companyInn': g.company.inn, 'userPin': g.user.inn, 'username': g.user.username}
    r = eds_post('company', data=params)
    if not c:
        c = Company.get_by_first(ref_id=r['id'])
    if c:
        r['company_status'] = c.company_status
        cq = CompanyQualification.get_by_first(company_id=c.id)
        if cq:
            r['qualification'] = cq.to_json()
    return r


def qualification(bag):
    c = Company.get_by_first(ref_id=bag['id'])
    if not c:
        raise CoreException(RECORD_NOT_FOUND)
    if c.company_status not in [COMPANY_STATUS_DRAFT, COMPANY_STATUS_WAITING]:
        raise CoreException(GENERIC_ERROR, u'Неверный статус организации')
    if not bag.get('SvidReg') or len(bag['SvidReg']) == 0:
        raise CoreException(GENERIC_ERROR, u'Отстутсвует данные Свидетельство о регистрации')
    if bag['ownership_type'] == 'org' and not bag.get('Ustav') or len(bag['Ustav']) == 0:
        raise CoreException(GENERIC_ERROR, u'Отстутсвует Устав')
    if not bag.get('SvedOpost') or len(bag['SvedOpost']) == 0:
        raise CoreException(GENERIC_ERROR, u'Отстутсвует Сведения о выполненных поставках товаров')
    if not bag.get('BuhBalance') or len(bag['BuhBalance']) == 0:
        raise CoreException(GENERIC_ERROR, u'Отстутсвует Бухгалтерский баланс и ЕНД')

    cq = CompanyQualification.get_by_first(company_id=c.id)
    if not cq:
        cq = CompanyQualification()
        cq.company_id = c.id
    cq.data = bag
    flag_modified(cq, 'data')
    cq.save()
    c.company_status = COMPANY_STATUS_WAITING
    c.save()

    return bag


def qualify(bag):
    today = datetime.now()
    c = Company.get(bag['company_id'])
    cq = CompanyQualification.get_by_first(company_id=c.id)
    if not cq:
        raise CoreException(RECORD_NOT_FOUND)
    status = bag['status']
    if status == COMPANY_STATUS_DRAFT:
        reason = bag.get('reason')
        if not reason:
            raise CoreException(GENERIC_ERROR, 'Reject reason is not entered')
        cq.data['reject_reason'] = reason
        cq.data['rejected_user'] = g.user.id
        cq.data['approve_date'] = None
        cq.data['rejected_date'] = str(today.date())
    elif status == COMPANY_STATUS_CONFIRMED:
        cq.data['reject_reason'] = ''
        cq.data['rejected_user'] = None
        cq.data['rejected_date'] = None
        cq.data['approve_date'] = str(today.date())
    else:
        raise CoreException(GENERIC_ERROR, 'Undefined qualification status')
    c.company_status = status
    c.save()
    flag_modified(cq, 'data')
    cq.save()
