import logging
import traceback

import jwt
from flask import g
from sqlalchemy.orm.attributes import flag_modified

from src.common.Authentication import Auth
from src.common.constants import ROLES, ROLE_PURCHASER, ROLE_SUPPLIER
from src.common.error_codes import *
from src.common.exceptions import CoreException
from src.common.utils import isValidEmail, eds_post
from src.controller import company
from src.models import User, db, bcrypt


def save(data):
    id = data.get('id')
    if id:
        u = User.get(id)
        if 'password' in data and not data.get('password', ''):
            del data['password']
        u.update(data)
    else:
        username = data.get('username')
        if username:
            user_in_db = User.get_by_username(username)
            if user_in_db:
                raise CoreException(USER_ALREADY_EXISTS)
            if not isValidEmail(username):
                raise CoreException(NOT_VALID_EMAIL)

        u = User(data)
        u.save()
        db.session.flush()
    return u


def register(data):
    user = save(data)
    user_json = user.to_json()
    token = Auth.generate_token(user.id)
    return {'user': user_json, 'token': token}


def login(data):
    user_in_db = None
    if data['username']:
        user_in_db = User.get_by_username(data['username'])

    if not user_in_db:
        raise CoreException(USER_NOT_FOUND)

    if user_in_db.blocked:
        raise CoreException(USER_IS_BLOCKED)

    if not user_in_db.check_hash(data['password']):
        raise CoreException(WRONG_PASSWORD)

    token = Auth.generate_token(user_in_db.id)
    user_json = user_in_db.to_json()
    if user_json.get('password', False):
        user_json.pop('password')

    return {'user': user_json, 'token': token}


def keycloak_login(token):
    try:
        payload = jwt.decode(str(token), algorithms=['HS512', 'RS256'], verify=False)
        pin = payload['pin']
        tin = payload['tin']
        username = payload['preferred_username']
        ecp_user = get_ecp_user(pin, tin, username)
        user, company = save_ecp_user(ecp_user)
        if not user:
            raise CoreException(GENERIC_ERROR, u'User data not found')
        if not company:
            raise CoreException(GENERIC_ERROR, u'Company data not found')
        g.user = user
        g.company = company

        token = Auth.generate_token('{}@{}@{}'.format(pin, tin, username))
        u = user.to_json()
        if hasattr(u, 'password'):
            u.pop('password')
        return {'user': u, 'company': company, 'token': token}

    except jwt.ExpiredSignatureError as e1:
        raise CoreException(GENERIC_ERROR, u'Token expired')
    except jwt.InvalidTokenError:
        raise CoreException(GENERIC_ERROR, u'Invalid token')
    except Exception as e:
        logging.error(traceback.format_exc())
        raise CoreException(GENERIC_ERROR, e.message)


def get_ecp_user(pin, inn, username):
    data = {'userPin': pin, 'companyInn': inn, 'username': username}
    us = eds_post('user', data)

    role = us.get('role')
    if not role:
        raise CoreException(GENERIC_ERROR, u'Undefined Role of Token')

    role_type = role.get('roleType')
    if not role_type:
        raise CoreException(GENERIC_ERROR, u'Undefined Role of Token')

    if int(role_type) == 2:
        us['role_id'] = ROLE_PURCHASER
    elif int(role_type) == 1:
        us['role_id'] = ROLE_SUPPLIER
    else:
        raise CoreException(GENERIC_ERROR, u'Undefined Role of Token')

    us['username'] = username
    return us


def info(user_id=None):
    the_user = g.user.to_json() if not user_id else User.get(user_id).to_json()
    if the_user.get('password', False):
        the_user.pop('password')

    if not the_user:
        raise CoreException(USER_NOT_FOUND)

    return {'user': the_user}


def get_users():
    users = []
    for u in User.all():
        us = u.to_json()
        us['role'] = ROLES[u.role_id]
        users.append(us)
    return users


def change_password(data):
    user_id = g.user.id
    u = User.get(user_id)
    if not u.check_hash(data.get('old')):
        raise CoreException(GENERIC_ERROR, 'wrong current password')

    new1 = data.get('new1')
    new2 = data.get('new2')

    if not new1 or new1 != new2:
        raise CoreException(GENERIC_ERROR, 'new password error')

    u.password = bcrypt.generate_password_hash(new1, rounds=10).decode("utf-8")
    u.save()


def save_ecp_user(bag):
    inn = bag['personPin']
    u = User.get_by_first(inn=inn)
    if not u:
        data = {
            'inn': bag['personPin'],
            'username': bag['username'],
            'fullname': bag['fullName'],
            'phone': bag['mobilePhone'],
            'position': bag['position'],
            'email': bag['email'],
            'name': 'test',
            'surname': 'test',
            'password': '123123',
            'role_id': bag['role_id'],
            'data': {
                'role': bag['role']
            }
        }
        u = User(data)
    else:
        u.data['role'] = bag['role']
    u.save()
    db.session.flush()

    bag['user_id'] = u.id
    c = company.save(bag)

    return u, c


def update_data(bag):
    user_id = g.user.id
    u = User.get(user_id)
    data = u.data or {}
    data.update(bag.get('data', {}))
    u.data = data
    flag_modified(u, 'data')
    u.save()
