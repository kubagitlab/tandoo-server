from flask import g
from sqlalchemy import text, bindparam, and_, or_

from .product import get
from ..common.constants import DT_FORMAT_TZ, DIR_KEYS, DIR_NAMES, DT_FORMAT_TH
from ..common.utils import *
from ..controller.company import save as company_save
from ..models import *


def save(bag):
    cd = CompanyDocument()
    cd.from_json(bag)
    cd.save()


def get_debts(bag):
    sql = db.session.query(CompanyDocument, Company, DirDocument) \
        .outerjoin(Company, CompanyDocument.company_id == Company.id) \
        .outerjoin(DirDocument, CompanyDocument.dir_document_id == DirDocument.id)

    if hasattr(g, 'company'):
        sql = sql.filter(CompanyDocument.company_id == g.company.id)
    if bag.get('dir_document_id', ''):
        sql = sql.filter(CompanyDocument.dir_document_id == bag['dir_document_id'])

    debts = []
    sql = sql.order_by(CompanyDocument.created_date.desc())

    if 'limit' in bag:
        sql = sql.limit(bag['limit'])

    for cd, c, doc in sql.all():
        d = orm_to_json(cd)
        d['company'] = c.name
        d['inn'] = c.inn
        d['document'] = doc.name
        d['debt_status'] = u'Имеется' if cd.debt else u'Не имеется'
        debts.append(d)
    return debts


def send_request(bag):
    sf_doc = db.session.query(DirDocument).filter(DirDocument.data['type'].astext == 'sf').first()
    tax_doc = db.session.query(DirDocument).filter(DirDocument.data['type'].astext == 'tax').first()

    inn = bag.get('inn', '')
    p_resp = eds_post('debt', {'companyInn': inn})
    sf = prepare_comp_doc(p_resp.get('sf', {}), g.company.id, sf_doc.id, bag.get('advert_id', ''))
    save(sf)
    tax = prepare_comp_doc(p_resp.get('gns', {}), g.company.id, tax_doc.id, bag.get('advert_id', ''))
    save(tax)
    return get_debts(bag)


def prepare_comp_doc(bag, comp_id, doc_id, advert_id):
    if not bag:
        return None, None
    date_issued = get_datetime(bag.get('dateIssued').split('.')[0], DT_FORMAT_TZ, True)
    date_end = get_datetime(bag.get('date').split('.')[0], DT_FORMAT_TZ, True)
    data = {'portal_id': bag.get('id', 0)}
    if advert_id:
        data['advert_id'] = advert_id
    comp_doc = {'company_id': comp_id, 'dir_document_id': doc_id, 'date_start': date_issued,
                'date_end': date_end, 'issuer': bag.get('issuer', ''), 'debt': bag.get('flag', False),
                'data': data}
    return comp_doc


def get_company_info(data):
    inn = data.get('companyInn', '')
    pin = data.get('userPin', '')
    p_resp = eds_post('user', data={'companyInn': inn, 'userPin': pin})
    comp = company_save(p_resp)
    return comp


def update_company_info(bag):
    comp = Company.query.filter_by(inn=bag['inn']).first()
    if not comp:
        return {'company': 'does not exist'}
    comp.data = bag['data']
    comp.save()
    return comp


def get_catalog(bag):
    catalog = []
    sql = db.session.query(CompanyProduct, Product, SectionOkgz, DirSection, DirOkgz) \
        .join(Product, CompanyProduct.product_id == Product.id) \
        .outerjoin(SectionOkgz, Product.parent_id == SectionOkgz.id) \
        .outerjoin(DirSection, SectionOkgz.section_id == DirSection.id) \
        .outerjoin(DirOkgz, SectionOkgz.okgz_id == DirOkgz.id) \
        .filter(CompanyProduct.company_id == g.company.id) \
        .order_by(CompanyProduct.date_add.desc())

    for cp, p, so, ds, o in sql.all():
        pr = cp.to_json()
        pr['category'] = o.name if o else ''
        pr['product'] = p.to_json()

        pv_sql = db.session.query(ProductValue, Entity) \
            .join(Entity, ProductValue.entity_id == Entity.id) \
            .filter(ProductValue.product_id == p.id, Entity.spr == True)
        for pv, e in pv_sql.all():
            if e.dir_name in DIR_NAMES:
                pr[DIR_KEYS[e.dir_name]] = get_dir_value(e.dir_name, pv.value_id)
        catalog.append(pr)

    return catalog


def save_product(bag):
    date_end = datetime.datetime.strptime(bag['date_end'], DT_FORMAT_TH)
    if date_end > datetime.datetime.now() + timedelta(days=60):
        raise CoreException('Дата не должна превышать 60 дней')
    cp = db.session.query(CompanyProduct) \
        .filter(CompanyProduct.company_id == g.company.id, CompanyProduct.product_id == bag['product_id']).first()
    if not cp:
        cp = CompanyProduct()
        cp.company_id = g.company.id
        cp.product_id = bag['product_id']
    cp.date_end = date_end
    cp.unit_price = float(bag['unit_price'])
    cp.save()
