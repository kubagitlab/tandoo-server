from src.models import ProductValue


def all(data):
    product_id = data.get('product_id')
    return ProductValue.get_by(product_id=product_id) if product_id else ProductValue.all()


def get(data):
    return ProductValue.get(data.get('id'))
