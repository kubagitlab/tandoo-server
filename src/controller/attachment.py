import logging
import os

from PIL import Image
from flask import g
from werkzeug.utils import secure_filename

from src.common.error_codes import NOT_ALLOWED_FILE_TYPE, GENERIC_ERROR
from src.common.exceptions import CoreException
from src.common.utils import allowed_file, get_path, allowed_img_file
from src.models import Attachment, db


def save(files, form):
    file_type = form['file_type']

    if 'file' not in files:
        CoreException(GENERIC_ERROR, 'No file part')
    file = files['file']
    if file.filename == '':
        CoreException(GENERIC_ERROR, 'No selected file')

    if not allowed_file(file.filename):
        raise CoreException(NOT_ALLOWED_FILE_TYPE)
    filename = secure_filename(file.filename)

    a = Attachment({'file_name': filename, 'file_type': file_type})
    a.user_id = g.user.id
    a.save()
    db.session.flush()

    file_path, thumb_path = get_path(file_type, a.saved_name)
    file.save(file_path)

    if allowed_img_file(a.file_name):
        try:
            base_width = 256
            img = Image.open(file_path)
            w_percent = (base_width / float(img.size[0]))
            h_size = int((float(img.size[1]) * float(w_percent)))
            img = img.resize((base_width, h_size), Image.ANTIALIAS)
            img.save(thumb_path)
        except IOError as ex:
            logging.info(u'attachment.save, Exception:{0}'.format(ex))
            raise CoreException(GENERIC_ERROR, 'File save Error')

    return a


def delete(id):
    a = Attachment.get(id)
    file_path, thumb_path = get_path(a.file_type, a.saved_name)

    if os.path.exists(file_path):
        os.remove(file_path)

    if os.path.exists(thumb_path):
        os.remove(thumb_path)

    a.delete()
