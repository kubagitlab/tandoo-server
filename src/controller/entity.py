from src.common.error_codes import GENERIC_ERROR
from src.common.exceptions import CoreException
from src.controller import entity_value
from src.models import db, Entity, EntityValue, SectionOkgz, ProductValue


def all(data):
    parent_id = data.get('parent_id')
    return Entity.get_by(parent_id=parent_id) if parent_id else []


def get(data):
    return Entity.get(data.get('id'))


def delete(id):
    if not id:
        return
    pv = ProductValue.get_by_first(entity_id=id)
    if pv:
        raise CoreException(GENERIC_ERROR, 'Product attribute is linked: {}'.format(pv.id))
    entity = Entity.get(id)
    entity_value.delete_by_entity_id(entity.id)
    entity.delete()


def save(data):
    section_okgz = data['section_okgz']
    id = section_okgz.get('id')
    so = SectionOkgz.get(id) if id else SectionOkgz()
    so.from_json(section_okgz)
    so.save()
    db.session.flush()

    entities = data['entities']

    for entity in entities:
        e = Entity.get(entity['id']) if 'id' in entity else Entity()
        e.from_json(entity)
        if e.spr and not e.dir_name:
            raise CoreException(GENERIC_ERROR, u'Укажите справочник для поле ' + e.name)
        e.parent_id = so.id
        e.save()

        if e.spr:  # for spr the values will come from the Dict tables
            continue

        db.session.flush()

        if 'values' not in entity:
            raise CoreException(GENERIC_ERROR, u'Добавьте значения для поле ' + e.name)
        for value in entity['values']:
            value['entity_id'] = e.id
            v = EntityValue.get(value['id']) if 'id' in value else EntityValue(value)
            v.from_json(value)
            v.save()
            db.session.flush()
    return
