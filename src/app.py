import logging
import logging.handlers
import os
import traceback
from datetime import timedelta
from time import time

from flask import Flask
from flask import request, g
from redis import Redis
from sqlalchemy.exc import IntegrityError

from .apis.api import api
from .apis.api_admin import api_admin
from .apis.api_con import api_con
from .apis.api_pur import api_pur
from .apis.api_sup import api_sup
from .common.Authentication import Auth
from .common.error_codes import USER_NOT_AUTHORIZED, ERRORS, SYSTEM_FAILURE, RECORD_NOT_FOUND
from .common.exceptions import CoreException
from .common.utils import make_json_response, get_config_type
from .controller.user import get_ecp_user
from .models import db, bcrypt, User, Company

log_exception = ['/user_info/']
log_censored = ['login', 'register']


def create_app():
    app = Flask(__name__)
    app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024  # 16 MB file upload size
    env_config = get_config_type()
    app.config.from_object(env_config)
    bcrypt.init_app(app)
    db.init_app(app)

    app.register_blueprint(api, url_prefix='/api')
    app.register_blueprint(api_admin, url_prefix='/api/admin')
    app.register_blueprint(api_sup, url_prefix='/api/sup')
    app.register_blueprint(api_pur, url_prefix='/api/pur')
    app.register_blueprint(api_con, url_prefix='/api/con')

    logger_init()

    logging.info(u'Started server {}'.format('TANDOO'))

    @app.before_request
    def before():
        if request.method == 'OPTIONS':
            return
        g.time = time()
        g.redis = Redis()
        for ex in log_exception:
            if request.url.endswith(ex):
                break
        else:
            msg = '{} Request: {} - Data: {}'
            for ex in log_censored:
                if request.url.endswith(ex):
                    data = '*censored*'
                    break
            else:
                data = request.data
            logging.info(
                msg.format(request.headers.get('X-Real-IP', request.remote_addr), request.url.encode('utf-8'), data))
        if hasattr(g, 'user'):
            g.user = None
        if 'api-token' in request.headers:
            token = request.headers.get('api-token')
            data = Auth.decode_token(token)
            if data['error']:
                raise CoreException(USER_NOT_AUTHORIZED, data['error'])

            check_user = None
            user_id = data['data']['user_id']
            us = str(user_id).split('@')
            if len(us) == 3:
                ecp_user = get_ecp_user(us[0], us[1], us[2])
                check_user = User.get_by_first(inn=ecp_user['personPin'])
            else:
                try:
                    check_user = User.get(user_id)
                except CoreException as ex:
                    if ex.code == RECORD_NOT_FOUND:
                        raise CoreException(USER_NOT_AUTHORIZED, u'Пользователь не найден')
                if check_user.blocked:
                    raise CoreException(USER_NOT_AUTHORIZED, u'Пользователь заблокирован')
            g.user = check_user
            g.redis.setex('online:' + str(check_user.id), timedelta(minutes=5), 'true')

            comp = Company().get_by_first(user_id=g.user.id)
            if comp:
                g.company = comp

    @app.teardown_request
    @app.errorhandler(Exception)
    @app.errorhandler(500)
    def after(exception):
        g.user = None
        if request.method == 'OPTIONS':
            return

        if hasattr(g, 'time'):
            logging.info('Request:{} finished in {} sec'.format(request.url.encode('utf-8'), time() - g.time))

        if exception:
            db.session.rollback()
            logging.exception(traceback.format_exc())
            if isinstance(exception, CoreException):
                return make_json_response({'message': exception.message, 'result': exception.code})

            try:
                message = exception.message
            except:
                message = str(exception)

            if isinstance(exception, KeyError):
                return make_json_response({'message': u'Не достаточно параметра {}'.format(message), 'result': -1})

            if isinstance(exception, IntegrityError):
                try:
                    m = exception.orig.pgerror
                    return make_json_response({'message': m, 'result': -1})
                except:
                    return make_json_response({'message': message, 'result': -1})

            return make_json_response({'message': ERRORS[SYSTEM_FAILURE], 'result': -1})

        else:
            db.session.commit()
        db.session.remove()

    @app.route('/', methods=['GET'])
    def index():
        return make_json_response({'response': 'SUCCESS!'})

    return app


def logger_init():
    path = 'log'
    os.makedirs(path, exist_ok=True)
    f = logging.Formatter(fmt='%(name)s; %(asctime)s; %(levelname)s; %(filename)s %(lineno)d: %(message)s ',
                          datefmt="%Y-%m-%d %H:%M:%S")
    handlers = [
        logging.handlers.TimedRotatingFileHandler(os.path.join(path, 'daily.log'), when="midnight", interval=1),
        logging.StreamHandler()
    ]
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    logging.getLogger('chardet.charsetprober').setLevel(logging.INFO)

    for h in handlers:
        h.setFormatter(f)
        h.suffix = "%Y%m%d"
        h.setLevel(logging.DEBUG)
        root_logger.addHandler(h)
