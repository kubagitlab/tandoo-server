import datetime

from flask import g
from sqlalchemy import func, Column, String, DateTime, Float, Integer, Boolean
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.sql import expression

from . import db, bcrypt
from .BaseModel import BaseModel
from ..common.json_encoder import orm_to_json
from ..common.utils import get_model_all


class User(BaseModel):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    surname = db.Column(db.String(128), nullable=False)
    father_name = db.Column(db.String(128))
    username = db.Column(db.String(128), unique=True)
    phone = db.Column(db.String, unique=True)
    inn = db.Column(db.String)
    fullname = db.Column(db.String)
    email = db.Column(db.String)
    position = db.Column(db.String)
    password = db.Column(db.String(128), nullable=False)
    role_id = db.Column(db.Integer, nullable=False)
    blocked = db.Column(db.Boolean, nullable=False, default=False)
    data = db.Column(JSONB, default={})

    def __init__(self, data):
        super().__init__(data)
        self.password = self.__generate_hash(data.get('password'))

    def update(self, data):
        for key, value in data.items():
            if key == 'password':
                self.password = self.__generate_hash(data[key])
            else:
                try:
                    setattr(self, key, value)
                except:
                    pass
        self.update_date = datetime.datetime.utcnow()
        db.session.add(self)

    @staticmethod
    def get_by_username(value):
        return User.query.filter_by(username=value).first()

    def __generate_hash(self, password):
        return bcrypt.generate_password_hash(password, rounds=10).decode("utf-8")

    def check_hash(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def __repr(self):
        return '<id {} {} {}>'.format(self.id, self.name, self.surname)

    def to_json(self):
        user_json = orm_to_json(self)
        user_json.pop('password')
        return user_json


class Attachment(BaseModel):
    __tablename__ = 'attachment'

    file_name = db.Column(db.String(128))
    file_type = db.Column(db.String(128))
    user_id = db.Column(db.Integer)

    @hybrid_property
    def saved_name(self):
        return str(self.id) + '_' + self.file_name


class DirOkgz(BaseModel):
    __tablename__ = 'okgz'

    id = db.Column(db.Integer, primary_key=True)
    ref_id = db.Column(db.String, nullable=False)
    name = db.Column(db.String(512), nullable=False)
    name_kg = db.Column(db.String(512))
    name_en = db.Column(db.String(512))
    code = db.Column(db.String(128))
    parent_id = db.Column(db.String(128))


class DirSection(BaseModel):
    __tablename__ = 'dir_section'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    name_kg = db.Column(db.String(128), unique=True)
    name_en = db.Column(db.String(128), unique=True)
    data = db.Column(JSONB, default={})


class SectionOkgz(BaseModel):
    __tablename__ = 'section_okgz'
    __table_args__ = (
        db.UniqueConstraint('section_id', 'okgz_id', name='section_okgz_idx'),
    )

    id = db.Column(db.Integer, primary_key=True)
    section_id = db.Column(db.Integer, db.ForeignKey(DirSection.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    okgz_id = db.Column(db.Integer, db.ForeignKey(DirOkgz.id, ondelete='cascade', onupdate='cascade'),
                        nullable=False, index=True)

    # order = db.Column(db.Integer, nullable=False)

    @hybrid_property
    def section(self):
        return DirSection.get(self.section_id)

    @hybrid_property
    def okgz(self):
        return DirOkgz.get(self.okgz_id)


class Entity(BaseModel):
    __tablename__ = 'entity'
    __table_args__ = (
        db.UniqueConstraint('parent_id', 'order', name='entity_order_idx'),
    )

    id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.Integer, db.ForeignKey(SectionOkgz.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    order = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    dir_name = db.Column(db.String(128))
    spr = db.Column(db.Boolean, server_default=expression.false(), nullable=False)
    mandatory = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
    guest = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
    purchaser = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
    supplier = db.Column(db.Boolean, server_default=expression.true(), nullable=False)

    @hybrid_property
    def values(self):
        if self.spr:
            return get_model_all(self.dir_name)
        else:
            return EntityValue.get_by(entity_id=self.id)


class EntityValue(BaseModel):
    __tablename__ = 'entity_value'
    __table_args__ = (
        db.UniqueConstraint('entity_id', 'order', name='entity_value_order_idx'),
    )

    id = db.Column(db.Integer, primary_key=True)
    entity_id = db.Column(db.Integer, db.ForeignKey(Entity.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    order = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))


class Product(BaseModel):
    __tablename__ = 'product'
    __table_args__ = (
        db.UniqueConstraint('parent_id', 'order', name='product_order_idx'),
    )

    id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.Integer, db.ForeignKey(SectionOkgz.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    order = db.Column(db.Integer, nullable=True)
    barcode = db.Column(db.String(128), unique=True)
    active = db.Column(db.Boolean, server_default=expression.true(), nullable=False)
    image = db.Column(db.String(166))
    images = db.Column(JSONB, nullable=False, default=[])
    local = db.Column(db.Boolean, server_default=expression.false(), nullable=False)
    user_id = db.Column(db.Integer)

    @hybrid_property
    def values(self):
        return ProductValue.get_by(product_id=self.id)

    @hybrid_property
    def company_product(self):
        if hasattr(g, 'user') and g.user:
            if hasattr(g, 'company'):
                return CompanyProduct.get_by_first(product_id=self.id, company_id=g.company.id)
            else:
                return CompanyProduct.get_by_first(product_id=self.id)
        return None


class ProductValue(BaseModel):
    __tablename__ = 'product_value'
    __table_args__ = (
        db.UniqueConstraint('product_id', 'entity_id', name='product_entity_idx'),
    )

    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(Product.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    # order = db.Column(db.Integer, nullable=False)
    entity_id = db.Column(db.Integer, db.ForeignKey(Entity.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    value_id = db.Column(db.Integer, nullable=True, index=True)


class DirCountry(BaseModel):
    __tablename__ = 'dir_country'

    id = db.Column(db.Integer, primary_key=True)
    ref_id = db.Column(db.Integer, nullable=False, unique=True)
    name = db.Column(db.String(128), nullable=False)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    iso_code = db.Column(db.String(128))
    sorting = db.Column(db.String(128))


class DirMeasureUnit(BaseModel):
    __tablename__ = 'dir_measure_unit'

    id = db.Column(db.Integer, primary_key=True)
    ref_id = db.Column(db.Integer, nullable=False, unique=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    short_name = db.Column(db.String(128))
    short_name_kg = db.Column(db.String(128))
    short_name_en = db.Column(db.String(128))
    type = db.Column(db.String(128))
    group = db.Column(db.String(128))


class DirBrand(BaseModel):
    __tablename__ = 'dir_brand'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    data = db.Column(JSONB, default={})


class DirBank(BaseModel):
    __tablename__ = 'dir_bank'

    id = db.Column(db.Integer, primary_key=True)
    ref_id = db.Column(db.Integer, nullable=False, unique=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    address = db.Column(db.String(128))
    phoneNumber = db.Column(db.String(128))
    head = db.Column(db.String(128))
    numberOfSubDivisions = db.Column(db.String(128))
    email = db.Column(db.String)
    website = db.Column(db.String(128))
    data = db.Column(JSONB, default={})


class DirOwnership(BaseModel):
    __tablename__ = 'dir_ownership'

    id = db.Column(db.Integer, primary_key=True)
    ref_id = db.Column(db.Integer, nullable=False, unique=True)
    name = db.Column(db.String(128), nullable=False)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    type_owner = db.Column(db.String(128), nullable=False)
    data = db.Column(JSONB, default={})


class DirManufacture(BaseModel):
    __tablename__ = 'dir_manufacture'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    data = db.Column(JSONB, default={})


class DirDocument(BaseModel):
    __tablename__ = 'dir_document'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    data = db.Column(JSONB, default={})


class DirPosition(BaseModel):
    __tablename__ = 'dir_position'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    name_kg = db.Column(db.String)
    name_en = db.Column(db.String)
    order = db.Column(db.Integer)
    data = db.Column(JSONB, default={})


class DirPayment(BaseModel):
    __tablename__ = 'dir_payment'

    name = db.Column(db.String(128), nullable=False)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    data = db.Column(JSONB, default={})


class DirCoate(BaseModel):
    __tablename__ = 'dir_coate'

    name = db.Column(db.String(128), nullable=False)
    name_kg = db.Column(db.String(128))
    name_en = db.Column(db.String(128))
    center = db.Column(db.String)
    code = db.Column(db.String, nullable=False)
    parent_id = db.Column(db.Integer)
    data = db.Column(JSONB, default={})


# методы закупок procurementMethod
class DirProcurement(BaseModel):
    __tablename__ = 'dir_procurement'

    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String)
    data = db.Column(JSONB, default={})


# типы закупок orderType
class DirProcureType(BaseModel):
    __tablename__ = 'dir_procure_type'

    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String)
    data = db.Column(JSONB, default={})


# способы закупок orderFormat
class DirProcureFormat(BaseModel):
    __tablename__ = 'dir_procure_format'

    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String)
    data = db.Column(JSONB, default={})


# статусы закупок orderStatus
class DirProcureStatus(BaseModel):
    __tablename__ = 'dir_procure_status'

    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String)
    data = db.Column(JSONB, default={})


# метод прямого заключения singleSourceContract
class DirSingleSource(BaseModel):
    __tablename__ = 'dir_single_source'

    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String)
    data = db.Column(JSONB, default={})


# обоснование к методу прямого заключения singleSourceReason
class DirSingleSrcRsn(BaseModel):
    __tablename__ = 'dir_single_src_rsn'

    name = db.Column(db.String, nullable=False)
    code = db.Column(db.String)
    data = db.Column(JSONB, default={})


class DirEconomicActivity(BaseModel):
    __tablename__ = 'dir_economic_activity'

    code = db.Column(db.String, nullable=False)
    name = db.Column(db.String, nullable=False)
    name_kg = db.Column(db.String)
    name_en = db.Column(db.String)
    data = db.Column(JSONB, default={})


class Company(BaseModel):
    __tablename__ = 'company'

    id = db.Column(db.Integer, primary_key=True)
    ref_id = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String, nullable=False)
    short_name = db.Column(db.String)
    inn = db.Column(db.String, nullable=False)
    company_type = db.Column(db.String)
    company_status = db.Column(db.String)
    data = db.Column(JSONB, default={})


class CompanyDocument(BaseModel):
    __tablename__ = 'company_document'

    id = db.Column(db.Integer, primary_key=True)
    dir_document_id = db.Column(db.Integer, nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    date_start = db.Column(db.DateTime, nullable=False, default=func.now())
    date_end = db.Column(db.DateTime, nullable=False, default=func.now())
    issuer = db.Column(db.String, nullable=False)
    debt = db.Column(db.Boolean, default=False)
    data = db.Column(JSONB, default={})


class CompanyProduct(BaseModel):
    __tablename__ = 'company_product'
    __table_args__ = (
        db.UniqueConstraint('company_id', 'product_id', name='company_product_idx'),
    )

    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey(Product.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    unit_price = db.Column(db.Float, nullable=False)
    date_add = db.Column(db.DateTime, nullable=False, default=func.now())
    date_update = db.Column(db.DateTime, nullable=False, default=func.now())
    date_end = db.Column(db.DateTime, nullable=False, default=func.now())
    data = db.Column(JSONB, default={})


class CompanyQualification(BaseModel):
    __tablename__ = 'company_qualification'
    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    data = db.Column(JSONB, default={})


class Advert(BaseModel):
    __tablename__ = 'advert'

    id = db.Column(Integer, primary_key=True)
    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    code = Column(String)
    dir_section_id = db.Column(db.Integer, db.ForeignKey(DirSection.id, ondelete='cascade', onupdate='cascade'),
                               nullable=False, index=True)
    dir_procurement_id = Column(Integer)
    status = Column(String, nullable=False)
    published_date = Column(DateTime)
    update_date = Column(DateTime, nullable=False)
    step = Column(Float)
    start_date = Column(DateTime)
    deadline = Column(DateTime)
    reason = Column(String)
    guarantee = Column(Float)
    concession = Column(Float)
    advert_date = Column(JSONB, default={})
    data = Column(JSONB, default={})


class AdvertLot(BaseModel):
    __tablename__ = 'advert_lot'

    advert_id = db.Column(db.Integer, db.ForeignKey(Advert.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    dir_okgz_id = Column(Integer, db.ForeignKey(DirOkgz.id, ondelete='cascade', onupdate='cascade'),
                         nullable=False, index=True)
    dir_unit_id = Column(Integer, db.ForeignKey(DirMeasureUnit.id, ondelete='cascade', onupdate='cascade'),
                         nullable=False, index=True)
    dir_payment_id = Column(Integer)
    lot_id = Column(Integer)
    quantity = Column(Float, nullable=False)
    unit_price = Column(Float, nullable=False)
    budget = Column(Float, nullable=False)
    delivery_place = Column(String, nullable=False)
    estimated_delivery_time = Column(Integer, nullable=False)
    plan_id = Column(Integer)
    status = Column(String)
    reason = Column(String)
    data = Column(JSONB, default={})


class LotEntity(BaseModel):
    __tablename__ = 'lot_entity'

    advert_lot_id = Column(Integer, db.ForeignKey(AdvertLot.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    entity_id = db.Column(db.Integer, db.ForeignKey(Entity.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    entity_value_id = Column(Integer, db.ForeignKey(EntityValue.id, ondelete='cascade', onupdate='cascade'),
                             index=True)
    data = Column(JSONB, default={})


class LotSpec(BaseModel):
    __tablename__ = 'lot_specification'
    __table_args__ = (
        db.UniqueConstraint('advert_lot_id', 'entity_id', 'value_id', name='lot_entity_evalue_idx'),
    )

    advert_lot_id = Column(Integer, db.ForeignKey(AdvertLot.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    entity_id = db.Column(db.Integer, db.ForeignKey(Entity.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    value_id = Column(Integer, db.ForeignKey(EntityValue.id, ondelete='cascade', onupdate='cascade'),
                      index=True)


class LotDir(BaseModel):
    __tablename__ = 'lot_dir'
    __table_args__ = (
        db.UniqueConstraint('advert_lot_id', 'entity_id', 'value_id', name='lot_entity_value_idx'),
    )

    advert_lot_id = Column(Integer, db.ForeignKey(AdvertLot.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    entity_id = db.Column(db.Integer, db.ForeignKey(Entity.id, ondelete='cascade', onupdate='cascade'),
                          nullable=False, index=True)
    value_id = Column(Integer, nullable=False)


class Application(BaseModel):
    __tablename__ = 'application'

    advert_lot_id = Column(String, nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey(Company.id, ondelete='cascade', onupdate='cascade'),
                           nullable=False, index=True)
    company_product_id = Column(Integer, db.ForeignKey(CompanyProduct.id, ondelete='cascade', onupdate='cascade'),
                                nullable=False, index=True)
    product_id = Column(Integer, db.ForeignKey(Product.id, ondelete='cascade', onupdate='cascade'),
                        nullable=False, index=True)
    unit_price = Column(Float, nullable=False)
    total = Column(Float, nullable=False)
    status = Column(String, nullable=False)
    lot_id = Column(Integer)
    code = Column(String)
    reason = Column(String)
    deadline = Column(DateTime, default=func.now())
    selected = Column(Boolean, default=False)
    signed = Column(Boolean)
    data = Column(JSONB, default={})
