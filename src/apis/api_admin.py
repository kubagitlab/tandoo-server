from flask import request, Blueprint, g

from ..common.Authentication import Auth
from ..common.constants import *
from ..common.error_codes import *
from ..common.exceptions import CoreException
from ..common.utils import make_json_response
from ..controller import dir, user, company

api_admin = Blueprint('api_admin', __name__)


@api_admin.before_request
def before_request():
    if request.method == 'OPTIONS':
        return
    if not hasattr(g, 'user') or not g.user:
        raise CoreException(USER_NOT_AUTHORIZED)
    if g.user.role_id not in [ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_OPERATOR]:
        raise CoreException(USER_LOW_ACCESS_LEVEL)


@api_admin.route('/', methods=['GET'])
def index():
    return make_json_response({'admin': True})


@api_admin.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    ret = user.register(data)
    return make_json_response(ret)


@api_admin.route('user/all', methods=['POST'])
@Auth.admin_required
def get_users():
    data = user.get_users()
    return make_json_response({"users": data})


@api_admin.route('user/save', methods=['POST'])
def save_user():
    data = request.get_json()
    r = user.save(data['user'])

    return make_json_response({"user": r})


@api_admin.route('change_password', methods=['POST'])
def change_password():
    data = request.get_json()
    user.change_password(data)
    return make_json_response()


@api_admin.route('dir/all', methods=['POST'])
def dir_all():
    return make_json_response(dir.all())


@api_admin.route('dir/save', methods=['POST'])
def save_dir():
    data = dir.save(request.get_json())
    return make_json_response(data)


@api_admin.route('dir/update', methods=['POST'])
def update_dir():
    data = dir.update(request.get_json())
    return make_json_response(data)


@api_admin.route('company/list', methods=['POST'])
def company_list():
    data = company.get_all()
    return make_json_response(data)


@api_admin.route('company/qualify', methods=['POST'])
def company_qualify():
    company.qualify(request.get_json())
    return make_json_response({})
