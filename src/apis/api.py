import logging
import os

from flask import Blueprint, send_file, request

from ..common.Authentication import Auth
from ..common.utils import make_json_response, get_path
from ..controller import user, okgz, dir, section_okgz, attachment
from ..controller.company import get_info_from_portal
from ..controller.product import get, products_by_category, get_sections
from ..controller.purchaser import published_adverts
from ..controller.user import update_data
from ..models import Attachment

api = Blueprint('api', __name__)


@api.route('/', methods=['GET'])
def index():
    return make_json_response({})


@api.route('/user_info', methods=['POST'])
@Auth.auth_required
def user_info():
    ret = user.info()
    return make_json_response(ret)


@api.route('/logout', methods=['POST'])
@Auth.auth_required
def logout():
    # TODO : blacklist token
    return make_json_response({})


@api.route('/login', methods=['POST'])
def login():
    data = request.get_json() or {}
    ret = user.login(data)
    return make_json_response(ret)


@api.route('/keycloak_login', methods=['POST'])
def keycloak_login():
    bag = request.get_json()
    ret = user.keycloak_login(bag.get('token', ''))
    return make_json_response(ret)


@api.route('dir/listing', methods=['POST'])
def dir_listing():
    bag = request.get_json()
    data = dir.listing(bag)
    return make_json_response(data)


@api.route('/dir/all', methods=['POST'])
def dir_all():
    ret = dir.all()
    return make_json_response(ret)


@api.route('/section_okgz/list', methods=['POST'])
def list_section_okgz():
    data = request.get_json()
    ret = section_okgz.all(data)
    return make_json_response(ret)


@api.route('/okgz/by_section', methods=['POST'])
def okgz_by_section():
    data = request.get_json()
    ret = section_okgz.get_okgz(data)
    return make_json_response(ret)


@api.route('/okgz/search', methods=['POST'])
def okgz_search():
    ret = okgz.search(request.get_json())
    return make_json_response(ret)


@api.route('upload', methods=['POST'])
@Auth.auth_required
def upload():
    f = attachment.save(request.files, request.form)
    return make_json_response({'file': f})


@api.route('delete_file', methods=['POST'])
@Auth.auth_required
def delete_file():
    data = request.get_json()
    attachment.delete(data['id'])
    return make_json_response()


@api.route('/file/<string:file_id>')
def get_file(file_id):
    try:
        a = Attachment.get(file_id)
    except:
        return 'File not found', 404

    file_path, thumb_path = get_path(a.file_type, a.saved_name)

    if not os.path.isfile(file_path):
        return 'File not found', 404

    try:
        return send_file(file_path, as_attachment=True, attachment_filename=a.file_name)
    except Exception as ex:
        logging.info(u'MAIN.get_file, Exception:{0}'.format(ex))
    return 'File not found', 404


@api.route('/thumb/<string:file_id>')
def get_file_thumb(file_id):
    try:
        a = Attachment.get(file_id)
    except:
        return 'File not found', 404

    file_path, thumb_path = get_path(a.file_type, a.saved_name)

    if not os.path.isfile(thumb_path):
        return 'File not found', 404

    try:
        return send_file(thumb_path, as_attachment=True, attachment_filename=a.file_name)
    except Exception as ex:
        logging.info(u'MAIN.get_file_thumb, Exception:{0}'.format(ex))
    return 'File not found', 404


@api.route('/product/by_category', methods=['POST'])
def product_by_category():
    data = request.get_json()
    return make_json_response(products_by_category(data))


@api.route('/product/get', methods=['POST'])
def get_product():
    data = request.get_json()
    pr = get(data['id'])
    return make_json_response({'product': pr})


@api.route('/section/by_products', methods=['POST'])
def section_by_products():
    return make_json_response(get_sections())


@api.route('/company/info', methods=['POST'])
@Auth.auth_required
def get_company_info():
    ret = get_info_from_portal(request.get_json())
    return make_json_response({'info': ret})


@api.route('/user/save_data', methods=['POST'])
@Auth.auth_required
def save_user_data():
    update_data(request.get_json())
    ret = user.info()
    return make_json_response({'info': ret})


@api.route('/advert/published', methods=['POST'])
def published_advert_list():
    return make_json_response({'adverts': published_adverts(request.get_json())})
