from flask import Blueprint, request, g

from ..common.Authentication import ROLE_SUPER_ADMIN, ROLE_CONSTRUCTOR
from ..common.error_codes import USER_LOW_ACCESS_LEVEL, USER_NOT_AUTHORIZED
from ..common.exceptions import CoreException
from ..common.utils import make_json_response
from ..controller import entity, entity_value, product, product_value
from ..controller.section_okgz import get_entities

api_con = Blueprint('api_con', __name__)


@api_con.before_request
def before_request():
    if request.method == 'OPTIONS':
        return
    if not hasattr(g, 'user') or not g.user:
        raise CoreException(USER_NOT_AUTHORIZED)
    if g.user.role_id not in [ROLE_SUPER_ADMIN, ROLE_CONSTRUCTOR]:
        raise CoreException(USER_LOW_ACCESS_LEVEL)


@api_con.route('/', methods=['GET'])
def index():
    return make_json_response({'operator': True})


@api_con.route('/entity/all', methods=['POST'])
def all():
    data = request.get_json()
    ret = entity.all(data)
    return make_json_response(ret)


@api_con.route('/section_okgz_entities', methods=['POST'])
def section_okgz_entities():
    data = request.get_json()
    ret = get_entities(data)
    return make_json_response(ret)


@api_con.route('/entity/save', methods=['POST'])
def save_entity():
    data = request.get_json()
    entity.save(data)
    return make_json_response()


@api_con.route('/entity/delete', methods=['POST'])
def delete_entity():
    data = request.get_json()
    entity.delete(data['id'])
    return make_json_response()


@api_con.route('/entity_value/delete', methods=['POST'])
def delete_entity_value():
    data = request.get_json()
    entity_value.delete(data['id'])
    return make_json_response()


@api_con.route('/entityvalue/list', methods=['POST'])
def list_value():
    data = request.get_json()
    ret = entity_value.all(data)
    return make_json_response(ret)


@api_con.route('/product/list', methods=['POST'])
def list_product():
    data = request.get_json()
    ret = product.all(data)
    return make_json_response(ret)


@api_con.route('/product/save', methods=['POST'])
def save_product():
    data = request.get_json()
    product.save(data)
    return make_json_response()


@api_con.route('/productvalue/list', methods=['POST'])
def list_product_value():
    data = request.get_json()
    ret = product_value.all(data)
    return make_json_response(ret)


@api_con.route('/product/get', methods=['POST'])
def get_product_value():
    data = request.get_json()
    ret = product.get(data)
    return make_json_response(ret)
