from flask import Blueprint, g, request

from ..common.constants import *
from ..common.error_codes import *
from ..common.exceptions import CoreException
from ..common.utils import make_json_response
from ..controller.product import company_products
from ..controller.purchaser import save_advert, advert_list, get_advert, update_advert_status

api_pur = Blueprint('api_pur', __name__)


@api_pur.before_request
def before_request():
    if request.method == 'OPTIONS':
        return
    if not hasattr(g, 'user') or not g.user:
        raise CoreException(USER_NOT_AUTHORIZED)
    if g.user.role_id not in [ROLE_PURCHASER]:
        raise CoreException(USER_LOW_ACCESS_LEVEL)


@api_pur.route('/', methods=['GET'])
def index():
    return make_json_response({'purchaser': True})


@api_pur.route('/product/list', methods=['POST'])
def product_list():
    return make_json_response({'data': company_products(request.get_json())})


@api_pur.route('/advert/save', methods=['POST'])
def advert_save():
    return make_json_response(save_advert(request.get_json()))


@api_pur.route('/advert/list', methods=['POST'])
def pur_advert_list():
    return make_json_response({'adverts': advert_list(request.get_json())})


@api_pur.route('/advert/get', methods=['POST'])
def advert_get():
    return make_json_response({'advert': get_advert(request.get_json())})


@api_pur.route('/advert/publish', methods=['POST'])
def advert_publish():
    params = request.get_json()
    params['status'] = ADVERT_STATUS_PUBLISHED
    return make_json_response(update_advert_status(params))
