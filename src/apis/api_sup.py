from flask import Blueprint, request

from src.controller.supplier import *
from ..common.constants import *
from ..common.error_codes import *
from ..common.exceptions import CoreException
from ..controller import company
from ..controller.company import get_info_from_portal

api_sup = Blueprint('api_sup', __name__)


@api_sup.before_request
def before_request():
    if request.method == 'OPTIONS':
        return
    if not hasattr(g, 'user') or not g.user:
        raise CoreException(USER_NOT_AUTHORIZED)
    if g.user.role_id not in [ROLE_SUPPLIER]:
        raise CoreException(USER_LOW_ACCESS_LEVEL)


@api_sup.route('/', methods=['GET'])
def index():
    return make_json_response({'supplier': True})


@api_sup.route('/debts', methods=['POST'])
def debts():
    data = request.get_json()
    return make_json_response({'debts': get_debts(data)})


@api_sup.route('/debt/send_request', methods=['POST'])
def debt_send_request():
    data = request.get_json()
    return make_json_response({'debts': send_request(data)})


@api_sup.route('/main_info', methods=['POST'])
def sup_main_info():
    data = request.get_json()
    return make_json_response({"sup_info": get_company_info(data)})


@api_sup.route('/update_info', methods=['POST'])
def update_info():
    data = request.get_json()
    return make_json_response({"sup_info": update_company_info(data)})


@api_sup.route('/catalog/list', methods=['POST'])
def catalog_list():
    data = request.get_json()
    return make_json_response({'catalog': get_catalog(data)})


@api_sup.route('/product/save', methods=['POST'])
def sup_save_product():
    save_product(request.get_json())
    return make_json_response()


@api_sup.route('/company/qualification', methods=['POST'])
def company_qualification():
    bag = request.get_json()
    r = company.qualification(bag)
    return make_json_response(r)
