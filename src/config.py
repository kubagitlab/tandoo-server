# /src/config.py

import os

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY', 'nO7EKNOcbAXGYKwvkv8hV5zz4gx1QKSZ')
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    API_ZAKUPKI = os.getenv('API_ZAKUPKI')
    ECP_JWT_SECRET_KEY = os.getenv('ECP_JWT_SECRET_KEY')
    TESTING = False
    DEBUG = False


class Development(Config):
    """
    Development environment configuration
    """
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Production(Config):
    """
    Production environment configurations
    """
    DEBUG = False


class Testing(Config):
    """
    Development environment configuration
    """
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'gartek_test.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PRESERVE_CONTEXT_ON_EXCEPTION = False


app_config = {
    'development': Development,
    'production': Production,
    'testing': Testing
}
