import time


def print_date_time():
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))
