import atexit
import os
import time

from apscheduler.schedulers.background import BackgroundScheduler
from dotenv import load_dotenv, find_dotenv
from flask_cors import CORS

from src.app import create_app

load_dotenv(find_dotenv())

app = create_app()

CORS(app, resources=r'*', supports_credentials=True,
     allow_headers=['Content-Type', 'api-token', 'Access-Control-Allow-Origin'])


def print_date_time():
    print(time.strftime("%A, %d. %B %Y %I:%M:%S %p"))
    # print(1/0)


# scheduler = BackgroundScheduler()
# scheduler.add_job(func=print_date_time, trigger="interval", minutes=1)
# scheduler.start()
#
# # Shut down the scheduler when exiting the app
# atexit.register(lambda: scheduler.shutdown())

if __name__ == '__main__':
    port = os.getenv('PORT')
    app.run(host='0.0.0.0', port=port)
