# install from requirements.txt
```pip install -r requirements.txt```

#Create a virtual environment for a project:
```$ pip install virtualenv```

```$ cd project_folder```

```$ virtualenv venv```

#activate virtualenv
```source venv/bin/activate #activate```

#activate virtualenv
```deactivate #deactivate```

#DB Migration flask_migrate

creates folder structures

```python manage.py db init ``` 

 populates the migration script with the detected changes in the models

```python manage.py db migrate``` 

 apply the migration to the database
 
```python manage.py db upgrade``` 


# run init data
```python manage.py init_db```