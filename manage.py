import logging

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from src.app import create_app, db
from src.common.constants import ROLE_SUPER_ADMIN, ROLE_CONSTRUCTOR
from src.controller import user

app = create_app()

migrate = Migrate(app=app, db=db)

manager = Manager(app=app)
manager.add_command('db', MigrateCommand)


@manager.command
def init_db():
    print('init start')

    logging.info('generating roles')

    logging.info('generating admin user')
    user.register(
        {"username": "admin@admin.kg",
         "name": "admin",
         "surname": "adminov",
         "password": "123123",
         "role_id": ROLE_SUPER_ADMIN}
    )
    user.register(
        {"username": "oper@oper.kg",
         "name": "oper",
         "surname": "operator",
         "password": "123123",
         "role_id": ROLE_CONSTRUCTOR}
    )


if __name__ == '__main__':
    manager.run()
